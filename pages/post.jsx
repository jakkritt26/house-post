import Head from 'next/head'
import { Col, Row } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import NavBarTop from '../src/components/template/nav';

const Post = () => {
    return (
        <>
            <Head>
                <title>Post Page</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <NavBarTop />
            <Container className='pt-3'>
                <Row>
                    <Col md={12} className='text-center'>
                        <h1>Welcome Post Page.</h1>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Post