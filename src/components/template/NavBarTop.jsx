import { Nav } from "react-bootstrap"

const NavBarTop = () => {
    return (
        <Nav defaultActiveKey="/" as="ul" className="bg-light py-2">
            <Nav.Item as="li">
                <Nav.Link href="/">หน้าแรก</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
                <Nav.Link eventKey="link-1" href="/post">ประกาศด่วน</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
                <Nav.Link eventKey="link-2">ติดต่อเรา</Nav.Link>
            </Nav.Item>
        </Nav>
    )
}

export default NavBarTop;