export const HTTP = {
    OK: 200,
    CREATE: 201,
    BAD_REQUEST: 400,
    UN_AUTHORIZE: 401,
    SERVER_ERROR: 500,
}